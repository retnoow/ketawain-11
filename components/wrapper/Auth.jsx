import React, {useEffect} from 'react'
import { useAuthState } from 'react-firebase-hooks/auth';
import { getAuth } from 'firebase/auth';
import firebaseConfig from '../../config/firebaseConfig';
import { useRouter } from 'next/router';
import nookies from 'nookies'

const Auth = ({children}) => {

    const auth = getAuth(firebaseConfig)
    const [user, loading=true] = useAuthState(auth)
    let router = useRouter()
    
    useEffect(() => {
      if(!user && !loading){
        router.replace('/login')
        nookies.destroy(null, 'token')
      } else if(user && !loading){
        user.getIdToken().then(token => nookies.set(undefined, 'token', token))
      }
           
    }, [user, loading, router])
    

    if(loading)
        return <h5>Checking...</h5>
    else if(user){
        return children;
    }
    
}

export default Auth