import React from "react";
import { getAuth } from "firebase/auth";
import firebaseApp from "../config/firebaseConfig";
import { useAuthState } from "react-firebase-hooks/auth";
import {
  getFirestore,
  updateDoc,
  arrayRemove,
  doc,
  arrayUnion,
} from "firebase/firestore";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFaceMeh } from "@fortawesome/free-regular-svg-icons";
import { faFaceLaughSquint } from "@fortawesome/free-solid-svg-icons";

const LikeJokes = ({ id, likes }) => {
  const auth = getAuth(firebaseApp);
  const liked = getFirestore(firebaseApp);
  const [user] = useAuthState(auth);

  const likesDoc = doc(liked, "jokes", id);

  const likesHandler = () => {
    if (likes?.includes(user.uid)) {
      updateDoc(likesDoc, {
        likes: arrayRemove(user.uid),
      })
        .then(() => {
          console.log("unliked");
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      updateDoc(likesDoc, {
        likes: arrayUnion(user.uid),
      })
        .then(() => {
          console.log("like");
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  const likeSymbol = () => {
    if (likes?.includes(user.uid)) {
      return <FontAwesomeIcon icon={faFaceLaughSquint} />
    } else {
      return <FontAwesomeIcon icon={faFaceMeh} />
    }
  }
  return (
    <div>
     
      <i
        className={`fa fa-heart${
          !likes?.includes(user.uid) ? "-o" : ""
        } fa-lg pe-3`}
        style={{
          cursor: "pointer",
          color: likes?.includes(user.uid) ? "dark" : null,
        }}
        onClick={likesHandler}
      >
        {likeSymbol()}
      </i>
    </div>
  );
};

export default LikeJokes;
