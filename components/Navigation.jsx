import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Nav, Navbar, Form } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import firebaseApp from "../config/firebaseConfig";
import { getAuth, signOut } from "firebase/auth";
import { useAuthState } from "react-firebase-hooks/auth";
import { useRouter } from "next/router";
import { toggleDarkMode } from "../store/reducers/DarkMode";
import Link from "next/link";

const Navigation = () => {
  const auth = getAuth(firebaseApp);
  const [user] = useAuthState(auth);

  useEffect(() => {
    console.log(user);
  }, [user]);

  const darkMode = useSelector((state) => state.darkMode.value);
  const dispatch = useDispatch();

  function Redirect({ to }) {
    const router = useRouter();
    useEffect(() => {
      router.push(to);
    }, [to, router]);
    return null;
  }

  const [shouldRedirect, setshouldRedirect] = useState(false);
  if (shouldRedirect) {
    return <Redirect to="/login" />;
  }

  return (
    <div className="">
      <Nav
        className={`navbar navbar-expand-lg navbar-light ${
          darkMode ? "bg-dark-2 text-light" : "bg-light-1 text-dark"
        }`}
      >
        <div className="container-fluid">
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className={"navbar-nav me-auto mb-2 mb-lg-0"}>
              <li className="nav-item">
                {
                  <Navbar.Brand className="nav-link active" aria-current="page">
                    <Link href="/" >
                      <a className="link-info fw-bold text-decoration-none"> KETAWA.IN</a>
                    </Link>
                  </Navbar.Brand>
                }
              </li>
              <li className="nav-item">
                {user && (
                  // <Nav.Link aria-current="page" href="dashboard">Dashboard</Nav.Link>
                  <Link href="dashboard">
                    <a className="nav-link"> Dashboard</a>
                  </Link>
                )}
              </li>
            </ul>
            <ul className="navbar-nav me-2 mb-2 mb-lg-0">
              <li className="nav-item">
                {!user && (
                  // <Nav.Link aria-current="page" href="register">Register</Nav.Link>
                  <Link href="register">
                    <a className="nav-link"> Register</a>
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {!user && (
                  // <Nav.Link aria-current="page" href="login">Log in</Nav.Link>
                  <Link href="login">
                    <a className="nav-link"> Login</a>
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {user && (
                  <div className="d-flex align-items-center">
                    <p className="mb-0">
                      {user.displayName || user.email.split("@")[0]}
                    </p>
                    <Nav.Link onClick={() => signOut(auth)}>Logout</Nav.Link>
                  </div>
                )}
              </li>
            </ul>
          </div>
        </div>
      </Nav>
      <div className="d-flex justify-content-end">
        <Form.Check
          className={`${darkMode && "text-light"} me-3 mt-3`}
          type="switch"
          id="custom-switch"
          label="Dark Mode"
          onChange={(e) => dispatch(toggleDarkMode(e.target.checked))}
        />
      </div>
    </div>
  );
};

export default Navigation;
