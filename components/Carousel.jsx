import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import Firestore from "../pages/firestore";
import firebaseApp from "../config/firebaseConfig";
import { useCollectionOnce } from "react-firebase-hooks/firestore";
import { getFirestore, collection, doc } from "firebase/firestore";
import { Image, Badge } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import React from "react";
import Slider from "react-slick";

export default function Carousel() {
  const darkMode = useSelector((state) => state.darkMode.value);

  var settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    arrows: false,
    slidesToScroll: 1,
  };
  const [value, loading, error] = useCollectionOnce(
    collection(getFirestore(firebaseApp), "jokes"),
    { getOptions: { source: "server" } }
  );

  const VIDEO_EXTS = ['mp4', 'mkv', 'avi']
  const IMAGE_EXTS = ['jpg', 'png', 'jpeg', 'gif']
  return (
    <div style={{ minHeight: "100vh", maxWidth: "100vw" }}>
      {error && <strong>Error: {JSON.stringify(error)}</strong>}
      {loading && <span>Collection: Loading...</span>}
      {value && (
        <Slider {...settings}>
          {value.docs.map((doc, index) => (
            // eslint-disable-next-line react/jsx-key
            <div className="d-flex flex-row justify-content-center align-items-center p-5 " key={index}>
              <div className="d-flex flex-column me-4">
              {VIDEO_EXTS.includes(doc.data().mediaType) && 
                <video width="380" height="340" controls>
                    <source src={doc.data().imageUrl} type="video/mp4" />
                        Your browser does not support the video tag.
                </video>
              }
              {IMAGE_EXTS.includes(doc.data().mediaType) && 
                <Image
                  src={doc.data().imageUrl}
                  alt="image"
                  style={{ height: 280, width: 380 }}
                            />
              }
                <Badge bg="primary" className="" style={{ fontSize: "13px" }}>
                  Well known as, {doc.data().username.toUpperCase()}
                </Badge>
              </div>

              <div className="d-flex col align-self-center px-2">
                <h3
                  className={`overflow-auto col-9 ${darkMode ? "text-light" : "text-dark"}` } 
                  style={{ maxHeight: "70vh", width: "70vw" }}
                >
                  {doc.data().description}
                </h3>
              </div>
            </div>
          ))}
        </Slider>
      )}
    </div>
  );
}
