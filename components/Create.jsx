import React, {useState, useRef} from 'react'
import { Col, Row, Form, Spinner } from 'react-bootstrap'
import { Modal, Button } from 'react-bootstrap'
import firebaseApp from '../config/firebaseConfig'
import { doc, setDoc, collection, getFirestore, addDoc } from 'firebase/firestore'

const Create = () => {

    const [show, setShow] = useState(false)
    const handleClose = () => setShow(false)
    const handleShow = () => setShow(true)

    const defaultJokesObj = {
        username: '',
        description: '',
        imageUrl: '',
        likes:[]
    }

    const [jokes, setJokes] = useState({...defaultJokesObj})


    const [file, setFile] = useState(null)
    const [loading, setLoading] = useState(false)
    const [isUploading, setisUploading] = useState(false)

    const createDoc = async () => {
        
        const db= getFirestore(firebaseApp)
        const jokesRef= collection(db, 'jokes')
        const upload = await mediaHandler()
        try {

            if(!jokes.username || !jokes.description){
                window.confirm("Please, complete all the fields")    
            } else{
                setLoading(true)
                await setDoc(doc(jokesRef),{...jokes, imageUrl: upload.secure_url,  mediaType: upload.format})
                window.confirm("Your jokes is posted")  
            }

          
        } catch (err) {
            console.log(err.message)
        }
        setLoading(false)
        handleClose(false)
    }


    const mediaHandler = async () => {
        setisUploading(true)
        const url = "https://api.cloudinary.com/v1_1/dxhjeszz5/auto/upload"

        const formData = new FormData()
        formData.append('file', file)
        formData.append('upload_preset', 'pnh6xjgi')

        try{
            const uploadFile = await fetch(url, {
                method: 'POST',
                body: formData
            })
            const data = await uploadFile.json()
            console.log(data)
            setisUploading(false)
            setFile(null) 
            
            return data
             
        }catch(err){
            console.log(err)
        }
    }

    const fileHandler = (event) => {
        setFile(event.target.files[0])
    }
    

  return (
    <div>
        <Button variant="primary" onClick={handleShow}>
            Create Jokes
        </Button>
        
        <Modal show={show} >
            <Modal.Header closeButton onClick={handleClose}>
                <Modal.Title>Express your jokes here, buddy!</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row className='justify-content-center'>
                    <Form className='mt-3'>
                        <Row className='g-3'>
                            <Col md={4}>
                                <Form.Label>Your AKA</Form.Label>
                                <Form.Control type="text" placeholder="Write your AKA"
                                onKeyDown={(e) => e.key === 'Enter' && createDoc() && setJokes({...defaultJokesObj})}
                                    onChange={(e) => setJokes({ ...jokes, username: e.target.value})} value={jokes.username} />
                            </Col>
                            <Col md={8}>
                                <Form.Label>Image Url</Form.Label>
                                <Form.Control type="file" onChange={fileHandler} />
                                
                            </Col>
                            <Col md={12}>
                                <Form.Label>Jokes</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="Write your jokes"
                                    style={{ height: '100px' }}
                                    onKeyDown={(e) => e.key === 'Enter' && createDoc() && setJokes({...defaultJokesObj})}
                                    onChange={(e) => setJokes({ ...jokes, description: e.target.value})} value={jokes.description} />
                            </Col>
                        </Row>
                    </Form>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>Close</Button>
                <Button variant="primary" 
                    disabled={loading || isUploading} 
                    type= "button" animation="grow" 
                    onClick={() => {createDoc() && setJokes({...defaultJokesObj})}}>
                    {isUploading && (
                        <Spinner
                        as="span"
                        animation="grow"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                        />
                     )}
                  {isUploading ? "Uploading..." : "Post"}
                </Button>
            </Modal.Footer>
        </Modal>

    </div>
  )
}

export default Create