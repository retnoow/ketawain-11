import React, {useState, useEffect} from 'react'
import {collection, getFirestore, deleteDoc} from 'firebase/firestore'
import firebaseApp from '../config/firebaseConfig'
import { useCollection } from 'react-firebase-hooks/firestore';
import { getAuth } from 'firebase/auth';
import { Image, Button, Badge, Col, Form } from 'react-bootstrap'
import { useAuthState } from 'react-firebase-hooks/auth';
import Container from 'react-bootstrap/Container';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import Create from '../components/Create'
import Like from '../components/Like';
import Navigation from '../components/Navigation'
import Update from '../components/Update';
import Auth from '../components/wrapper/Auth';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPoo } from '@fortawesome/free-solid-svg-icons';

const Dashboard = () => {
                       
  const [show, ] = useState(false)
  const auth = getAuth(firebaseApp)
  const [user, ] = useAuthState(auth)
  const [keyword, setKeyword] = useState('')
  const [file, setFile] = useState(null)
  const darkMode = useSelector((state) => state.darkMode.value);

  const [snapshot, ,] = useCollection(
    collection(getFirestore(firebaseApp), "jokes"),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  const search = snapshot?.docs.filter((item) =>
    keyword !== ""
      ? new RegExp(keyword, "g").test(item.data().username.toLowerCase()) ||
        new RegExp(keyword, "g").test(item.data().description.toLowerCase())
      : item
  );

  const DeleteHandler = ({ doc }) => {
    return (
      <Button variant="outline-danger" onClick={() => deleteDoc(doc.ref)}>
        <FontAwesomeIcon icon={faPoo} />
      </Button>
    );
  };

  const VIDEO_EXTS = ['mp4', 'mkv', 'avi']
  const IMAGE_EXTS = ['jpg', 'png', 'jpeg', 'gif']
 
  return (
    <div className={` ${darkMode ? "bg-dark-1" : "bg-light-3"}`} style={{minHeight: '100vh'}}>
      <Navigation />
      <div className="p-5">
        <div className="d-flex justify-content-end mb-4 mx-4">
          <Create show={show} />
        </div>

        <Col
          md={{ span: 6, offset: 3 }}
          className={` ${
            darkMode && "text-light"
          } d-flex align-items-center gap-3 mb-3 `}
        >
          <span className={`${darkMode ? "text-light" : "text-dark"}`}>Search</span>
          <Form.Control
            type="text"
            placeholder="Enter some keyword"
            onChange={(e) => setKeyword(e.target.value)}
          />
        </Col>

        {snapshot && snapshot.docs.length === 0 ? (
          <p
            style={{ fontSize: "20px", marginTop: "15%", textAlign: "center" }}
          >
            Empty jokes here
          </p>
        ) : (
          <Container className=" d-flex justify-content-center align-items-center mt-5">
            <div>
              {search?.map((doc) => (
                <div
                  className={` ${
                    darkMode ? "bg-dark text-light" : "bg-light"
                  } p-3 mb-4 bg-light border`}
                  key={doc.id}
                  style={{ width: "150vh" }}
                >
                  <div className="d-flex flex-row-reverse gap-3">
                    <DeleteHandler doc={doc} />
                    <Update item={doc.data()} itemId={doc.id}/>
                  </div>
                  <div className="row">
                    <div className="col-3">
                      <Link href={`/detail/${doc.id}`} query={snapshot?.docs}>          
                        <a>
                          {VIDEO_EXTS.includes(doc.data().mediaType) && 
                            <video width="280" height="240" controls>
                                <source src={doc.data().imageUrl} type="video/mp4" />
                                Your browser does not support the video tag.
                            </video>
                          }
                          {IMAGE_EXTS.includes(doc.data().mediaType) && 
                            <Image
                                src={doc.data().imageUrl}
                                alt="image"
                                style={{ height: 180, width: 180 }}
                            />
                          }
                        </a>
                      </Link>
                    </div>
                    <div className="col-9 ps-3">
                      <Badge
                        bg="warning"
                        className="mb-3"
                        style={{ fontSize: "13px" }}
                      >
                        Well known as, {doc.data().username.toUpperCase()}
                      </Badge>
                      <h5>{doc.data().description}</h5>
                      <div className="d-flex flex-row-reverse">
                        <div className="pe-3">
                          <p>{doc.data().likes?.length} Like</p>
                        </div>
                        {user && <Like id={doc.id} likes={doc.data().likes} />}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </Container>
          
        )}
      </div>
     
    </div>
  );
};

Dashboard.getLayout = (page) => <Auth>{page}</Auth>

export default Dashboard
