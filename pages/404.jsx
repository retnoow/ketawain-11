import React, { useEffect } from "react";
import { useRouter } from 'next/router';


const Error404 = () => {
    const router = useRouter();


    useEffect(() => {
      setTimeout(() => {
        router.push('/')
      }, 2000);
    
      
    }, [router])
    
  return (
    <div> <h1 className='title-not-found'>Halaman yang anda kunjungi tidak ada</h1></div>
  )
}

export default Error404