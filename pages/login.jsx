import React, { useState, useEffect } from "react";
import { Container, Row, Col, Button, Form, Alert } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";
import { useSignInWithEmailAndPassword, useSignInWithGoogle } from "react-firebase-hooks/auth";
import { getAuth } from "firebase/auth";
import firebaseApp from "../config/firebaseConfig";
import AuthLayout from "../components/layouts/AuthLayout";
import Router, { useRouter } from "next/router";
import Link from "next/link";

const errorDict = {
  "auth/wrong-password": "Password anda salah!",
  "auth/internal-error": "Password harus diisi!",
  "auth/invalid-email": "Email harus diisi!",
  "auth/user-not-found": "User tidak ditemukan!"
};

const Login = () => {
  const router = useRouter();
  const auth = getAuth(firebaseApp);
  const darkMode = useSelector((state) => state.darkMode.value);

  const [credentials, setCredentials] = useState({
    email: "",
    password: "",
  });

  const [signInWithEmailAndPassword, user, loading, error] =
    useSignInWithEmailAndPassword(auth);

  const [signInWithGoogle, userGoogle, , ] = useSignInWithGoogle(auth);

  useEffect(() => {
      if(user || userGoogle !== undefined)
          router.replace('/dashboard');
  }, [user, userGoogle, router])

  const loginHandler = () => {
    signInWithEmailAndPassword(credentials.email, credentials.password);
    console.log(auth.currentUser);
  };

  return (
    <AuthLayout>
      <Container className="d-flex justify-content-center align-items-center" style={{height: '90vh', width: '80vw'}}>
        <Row style={{ width: '80vw'}}>
          <Col className={`d-flex justify-content-center flex-column gap-3 bg-light-2 rounded p-5 ${darkMode ? "bg-dark-2" : "bg-light-1"}`}
            style={{ height: "auto" }}>
            <h1 className="mb-4">Login</h1>
            <Form>
              <Form.Group className="mb-3 fs-5" controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email address"
                  onKeyDown={(e) => e.key === "Enter" && loginHandler()}
                  onChange={(e) =>
                    setCredentials({ ...credentials, email: e.target.value })
                  }
                  value={credentials.email}
                />
              </Form.Group>

              <Form.Group className="mb-3 fs-5" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Password"
                  onKeyDown={(e) => e.key === "Enter" && loginHandler()}
                  onChange={(e) =>
                    setCredentials({ ...credentials, password: e.target.value })
                  }
                  value={credentials.password}
                />
              </Form.Group>
              <div className="d-flex justify-content-end ml-2 mt-4 mb-5">
                <Button
                  disabled={loading}
                  variant="primary"
                  type="button"
                  onClick={loginHandler}
                >
                  {loading ? "Loading..." : "Login"}
                </Button>

                <Button className="mx-2" variant="danger" type="button" onClick={() => signInWithGoogle()}>
                  Sign in with Google
                </Button>
              </div>
              {error && (
                <Alert variant="danger">{error && errorDict[error.code]}</Alert>
              )}

              <span className="d-flex justify-content-center align-items-center">Belum punya akun KETAWAIN? &nbsp;<Link  href="/register"><a className="text-decoration-none text-success">Register</a></Link></span>
            </Form>
          </Col>
          <Col
            style={{ height: "auto" }}
            className={`d-flex justify-content-center align-items-center flex-column gap-4 text-center bg-light-3 text-secondary ${darkMode ? "bg-dark-1" : "bg-light-3"}`}
          >
            <h2 nameclass="mt-5 mb-5">Jus apa yang turun dari langit?</h2>
            <h2 nameclass="mt-5 mb-5">Justru itu yang aku enggak tau</h2>
          </Col>
        </Row>
      </Container>
    </AuthLayout>
  );
};

export default Login;
