import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import store from '../store/store';
import { Provider } from 'react-redux';

function MyApp({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page)
  return (
  <Provider store={store}>
      {getLayout(<Component {...pageProps} />)}
  </Provider>

  )
}

export default MyApp

