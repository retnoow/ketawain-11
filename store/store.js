import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './reducers/CounterSlice'
import DarkModeReducer from './reducers/DarkMode'

export default configureStore({
  reducer: {
    counter: counterReducer,
    darkMode: DarkModeReducer
  },
})